import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyAsvwpkDJxv4kblQVtfyvJSnmnf8ejMt7o",
      authDomain: "udemy-recipe-1c39f.firebaseapp.com",
    })
  }
}
