import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from "./app/app.module";

export {AppServerModule} from './app/app.server.module';

enableProdMode();

platformBrowserDynamic().bootstrapModule(AppModule);