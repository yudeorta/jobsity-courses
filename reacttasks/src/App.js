import React, { Component } from 'react';
import { Appbar, Container, Button } from 'muicss/react';
import Tasks from './components/Tasks';
import AddTask from './components/AddTask';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      tasks: []
    }
  }

  componentWillMount() {
    this.getTasks()
  }

  getTasks() {
    axios.request({
      method: 'GET',
      url: 'https://api.mlab.com/api/1/databases/yudereacttasks/collections/tasks?apiKey=O5fukMEBisexpEj-fobWh6oUWBAxg3FR'
    }).then(( response ) => {
      this.setState({ tasks: response.data }, () => {
        console.log(this.state)
      })
    }).catch(( error ) => {
      console.log(error)
    })
  }

  editState( task, checked ) {
    axios.request({
      method: 'put',
      url: 'https://api.mlab.com/api/1/databases/yudereacttasks/collections/tasks/' + task._id.$oid + '?apiKey=O5fukMEBisexpEj-fobWh6oUWBAxg3FR',
      data: {
        text: task.text,
        completed: checked
      }
    }).then(( response ) => {
      let tasks = this.state.tasks;
      for( let i = 0; i < tasks.length; i++ ) {
        if( tasks[ i ]._id.$oid === response.data._id.$oid ) {
          tasks[ i ].completed = checked
        }
      }
      this.setState({ tasks: tasks })
    }).catch(( error ) => {
      console.log(error)
    })
  }

  addTask( text ) {
    axios.request({
      method: 'post',
      url: 'https://api.mlab.com/api/1/databases/yudereacttasks/collections/tasks?apiKey=O5fukMEBisexpEj-fobWh6oUWBAxg3FR',
      data: {
        text: text,
        completed: false
      }
    }).then(( response ) => {
      let tasks = this.state.tasks;
      tasks.push({
        _id: response.data._id,
        text: text,
        completed: false
      });
      this.setState({ tasks: tasks })
    }).catch(( error ) => {
      console.log(error)
    })
  }

  clearTasks() {
    let tasks = this.state.tasks;
    let i = tasks.length;

    while( i-- ) {
      if( tasks[ i ].completed === true ) {
        let id = tasks[ i ]._id.$oid;

        axios.request({
          method: 'delete',
          url: 'https://api.mlab.com/api/1/databases/yudereacttasks/collections/tasks/' + id + '?apiKey=O5fukMEBisexpEj-fobWh6oUWBAxg3FR',
        }).then(( response ) => {
          tasks.splice(i, 1);
          this.setState({tasks: tasks})
        }).catch(( error ) => {
          console.log(error)
        })
      }
    }
  }

  render() {
    return (
      <div className="App">
        <Appbar>
          <Container>
            <table width="100%">
              <tbody>
              <tr>
                <td className="mui--appbar-height">
                  <h3>ReactTasks</h3>
                </td>
              </tr>
              </tbody>
            </table>
          </Container>
        </Appbar>
        <br/>
        <Container>
          <AddTask onAddTask={this.addTask.bind(this)}/>
          <Tasks onEditState={this.editState.bind(this)} tasks={this.state.tasks}/>
          <Button color="danger" onClick={this.clearTasks.bind(this)}>Clear Tasks</Button>
        </Container>
      </div>
    );
  }
}

export default App;
