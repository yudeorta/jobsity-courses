import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class App extends Component {

  onLoginClick() {
    this.props.onLoginClick();
  }

  onLogoutClick() {
    this.props.onLogoutClick();
  }

  render() {
    let navItems = this.props.idToken ?
      <NavItem href="#" onClick={this.onLogoutClick.bind(this)}>Logout</NavItem> :
      <NavItem href="#" onClick={this.onLoginClick.bind(this)}>Login</NavItem>;

    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            ReactAuth App
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          {navItems}
        </Nav>
      </Navbar>
    );
  }
}

export default App;
